
##Relatorio Repository Api
Essa api está usando OAuth2 e JWT Token para autenticação

Com o propósito apenas de testar a api foi adicionado o usuário TesteApi senha TesteApi

endPoint para autenticação /api/login , para as demais chamadas adicionar o token no Header Authorization Bearer

no Swagger TryOut na Api Login e depois adicionar o token do usuário no Swagger Authorize

##EF Core
O Entity Framework foi adicionado ao projeto, caso a base de dados não exista por favor rodar o comando Update-Database 
no Package Manager Console

##String de conexão com o banco de dados
Configurável através dos arquivos ..\relatoriotransacoesapi\RelatorioTransacoesApi\appsettings.Development
..\relatoriotransacoesapi\RelatorioTransacoesApi\appsettings