﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RelatorioTransacoesApi.Model;
using RelatorioTransacoesApi.Model.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RelatorioTransacoesApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        ITransactionRepository _repositoryTransaction;

        public TransactionController(ITransactionRepository transactionRepository)
        {
            _repositoryTransaction = transactionRepository;
        }

        [HttpGet()]
        [Authorize("Bearer")]
        public async Task<ActionResult<IEnumerable<Transaction>>> GetTransactions([FromQuery] string cnpjs = null,[FromQuery] string datas = null,
                                                                                  [FromQuery] string bandeiras = null, [FromQuery]string adquirentes = null,
                                                                                  [FromQuery] int _periodo=0)
        {
            try
            {
                string[] cnpj = string.IsNullOrEmpty(cnpjs) ? null : cnpjs.Split(',');
                string[] data = string.IsNullOrEmpty(datas) ? null : datas.Split(',');
                string[] bandeira = string.IsNullOrEmpty(bandeiras) ? null : bandeiras.Split(',');
                string[] adquirente = string.IsNullOrEmpty(adquirentes) ? null : adquirentes.Split(',');
                int periodo = _periodo;

                var transactions = await _repositoryTransaction.Get().Where(x => (cnpj == null || cnpj.Contains(x.MerchantCnpj)) &&
                                                                    (data == null || data.Contains(x.CreatedAt.ToString("yyyy-MM-dd"))) &&
                                                                    (bandeira == null || bandeira.Contains(x.CardBrandName)) &&
                                                                    (adquirente == null || adquirente.Contains(x.AcquirerName)) &&
                                                                    (periodo==0 || x.CreatedAt>=DateTime.Now.AddDays(-periodo))).ToListAsync();

                return transactions;
            }
            catch
            {
                return null;
            }
        }

       
    }
}
