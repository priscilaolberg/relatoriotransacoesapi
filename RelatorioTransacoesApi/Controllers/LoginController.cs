﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using RelatorioTransacoesApi.Helper;
using RelatorioTransacoesApi.Model;
using RelatorioTransacoesApi.Model.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RelatorioTransacoesApi.Controllers
{
    [Route("api/[controller]")]
    public class LoginController : Controller
    {
        private Criptografia _cryptography;
        ILogger _logger;

        public LoginController(IConfiguration configuration, ILogger<LoginController> logger)
        {
            _cryptography = new Criptografia(configuration);
            _logger = logger;
        }


        [AllowAnonymous]
        [HttpPost]
        public ActionResult<Autenticacao> Post(
            [FromQuery]string login, 
            [FromQuery]string senha,
            [FromServices]IUsuarioApiRepository usuarioRepository,
            [FromServices]SigningConfigurations signingConfigurations,
            [FromServices]TokenConfigurations tokenConfigurations)
        {
            try
            {
                bool credenciaisValidas = false;
                UsuarioApi usuarioResult = new UsuarioApi();
                if (!string.IsNullOrWhiteSpace(login))
                {
                    senha = _cryptography.sha256(senha);
                    var usuarioBase = usuarioRepository.Get(u => u.Login == login).FirstOrDefault();
                    credenciaisValidas = (usuarioBase != null &&
                        login == usuarioBase.Login &&
                        senha == usuarioBase.Senha);
                    if (credenciaisValidas)
                        usuarioResult = usuarioBase;
                }

                if (credenciaisValidas)
                {
                    ClaimsIdentity identity = new ClaimsIdentity(
                        new GenericIdentity(login, "Login"),
                        new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.UniqueName, login)
                        }
                    );

                    DateTime dataCriacao = DateTime.Now;
                    DateTime dataExpiracao = dataCriacao +
                        TimeSpan.FromDays(tokenConfigurations.Days);

                    var handler = new JwtSecurityTokenHandler();
                    var securityToken = handler.CreateToken(new SecurityTokenDescriptor
                    {
                        Issuer = tokenConfigurations.Issuer,
                        Audience = tokenConfigurations.Audience,
                        SigningCredentials = signingConfigurations.SigningCredentials,
                        Subject = identity,
                        NotBefore = dataCriacao,
                        Expires = dataExpiracao
                    });
                    var token = handler.WriteToken(securityToken);

                    return new Autenticacao()
                    {
                        authenticated = true,
                        created = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                        expiration = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                        accessToken = token,
                        usuario = usuarioResult,
                        message = "OK"
                    };
                }
                else
                {
                    return new Autenticacao()
                    {
                        authenticated = false,
                        message = "Falha ao autenticar"
                    };
                }
            }
            catch(Exception ex)
            {
                _logger.LogError("Login", ex.Message);
                return new Autenticacao()
                {
                    authenticated = false,
                    message = "Falha ao autenticar"
                };
            }
        }
        
    }
}