﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RelatorioTransacoesApi.Model
{
    public class Transaction
    {
        [Key]
        public int TransactionId { get; set; }
        public string MerchantCnpj { get; set; }
        public string CheckoutCode { get; set; }
        public string CipheredCardNumber { get; set; }
        public string AmountInCent { get; set; }
        public string Installments { get; set; }
        public string AcquirerName { get; set; }
        public string PaymentMethod { get; set; }
        public string CardBrandName { get; set; }
        public string Status { get; set; }
        public string StatusInfo { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime AcquirerAuthorizationDateTime { get; set; }
    }
}