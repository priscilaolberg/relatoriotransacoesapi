﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RelatorioTransacoesApi.Model
{
    public class UsuarioApi
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        [NotMapped]
        public string accessToken { get; set; }
        public UsuarioApi()
        {
            Id = Guid.NewGuid();
        }
    }
}
