﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RelatorioTransacoesApi.Model.Repository
{
    public interface ITransactionRepository : IRepository<Transaction>
    {
    }
}
