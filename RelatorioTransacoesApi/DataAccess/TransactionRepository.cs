﻿using RelatorioTransacoesApi.DataAccess.Context;
using RelatorioTransacoesApi.Model;
using RelatorioTransacoesApi.Model.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RelatorioTransacoesApi.DataAccess
{
    public class TransactionRepository : Repository<Transaction>, ITransactionRepository
    {
        public TransactionRepository(TransactionContext context) : base(context)
        {

        }

        
    }
}

