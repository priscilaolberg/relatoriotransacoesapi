﻿using RelatorioTransacoesApi.Helper;
using RelatorioTransacoesApi.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.Text;
using CsvHelper;
using RelatorioTransacoesApi.DataAccess.Context;

namespace RelatorioTransacoesApi.DataAccess.DataSeed
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UsuarioApi>().HasData(
                new UsuarioApi
                {
                    Login = "TesteApi",
                    Senha = "15da3284f644d841b22aa9487740304af44cd02e3ff1a474aea49890226d8562"
                }
            );

            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "RelatorioTransacoesApi.DataAccess.DataSeed.transactions.csv";
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                {
                    CsvReader csvReader = new CsvReader(reader);
                    var transactions = csvReader.GetRecords<Transaction>().ToArray();
                    modelBuilder.Entity<Transaction>().HasData(transactions);
                }
            }


        }
    }
}
