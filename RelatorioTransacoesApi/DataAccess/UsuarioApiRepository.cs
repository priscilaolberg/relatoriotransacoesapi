﻿using RelatorioTransacoesApi.DataAccess.Context;
using RelatorioTransacoesApi.Model;
using RelatorioTransacoesApi.Model.Repository;
using Microsoft.Extensions.Configuration;

namespace RelatorioTransacoesApi.DataAccess
{
    public class UsuarioApiRepository : Repository<UsuarioApi>, IUsuarioApiRepository
    {
        public UsuarioApiRepository(TransactionContext context) : base(context)
        {
        }

    }
}