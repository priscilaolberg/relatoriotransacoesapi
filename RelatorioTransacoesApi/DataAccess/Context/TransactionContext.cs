﻿using RelatorioTransacoesApi.DataAccess.DataSeed;
using RelatorioTransacoesApi.Model;
using Microsoft.EntityFrameworkCore;

namespace RelatorioTransacoesApi.DataAccess.Context
{
    public class TransactionContext : DbContext
    {
        public TransactionContext(DbContextOptions<TransactionContext> options)
            : base(options)
        {
        }

        public DbSet<UsuarioApi> UsuarioApi { get; set; }
        public DbSet<Transaction> Transaction { get; set; }
       
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Seed();
            base.OnModelCreating(modelBuilder);

        }

    }
}
